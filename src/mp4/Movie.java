package mp4;

public class Movie {
	private final int id;
	private final String name;
	private final int releaseYear;
	private final String imdbUrl;

	/**
	 * Create a new Movie object with the given information.
	 * 
	 * @param id
	 *            the movie id
	 * @param name
	 *            the name of the movie
	 * @param releaseYear
	 *            the year of the movie's release
	 * @param imdbUrl
	 *            the movie's IMDb URL
	 */
	public Movie(int id, String name, int releaseYear, String imdbUrl) {
		this.id = id;
		this.name = name;
		this.releaseYear = releaseYear;
		this.imdbUrl = imdbUrl;
	}

	/**
	 * Return the name of the movie
	 * 
	 * @return movie name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the release year of the movie
	 */
	private int getReleaseYear() {
		return this.releaseYear;
	}
	
	/**
	 * hashCode for equality testing
	 */
	@Override
	public int hashCode() {
		return id;
	}
	
	/**
	 * Returns this movie's ID. For readability in MovieGraph code.
	 */
	public int id() {
		return id;
	}
	
	/**
	 * Method to check if two Movie objects are equal. Two movie objects
	 * m1 and m2 are considered equal iff (m1.id == m2.id) and 
	 * (m1.releaseYear == m2.releaseYear) and (m1.name.equals(m2.name))
	 */
	@Override
	public boolean equals(Object other) {
		if( !(other instanceof Movie) ) {
			return false;
		}
		Movie otherMovie = (Movie) other;
		if( otherMovie.hashCode() != this.hashCode() ) {
			return false;
		}
		if( this.getName().equals(otherMovie.getName()) && this.getReleaseYear() == otherMovie.getReleaseYear() ) {
			return true;
		}
		return false;
	}
	
}
