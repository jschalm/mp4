package mp4;

/*
 * Represents a square matrix with n entries.
 * RI:	Number of rows must always equal number of columns.
 * AF:	A Matrix object corresponds to a nxn integer matrix with n = matrix.size() 
 * 		such that matrix(i,j) = the entry at the ith row and jth column of the matrix.
 * 		
 *
 */
public class Matrix {
	private int[][] matrix;
	
	/**
	 * Standard constructor method for the Matrix object. Initializes the 
	 * matrix with a default size of 10, with all entries equal to zero.
	 */
	public Matrix() {
		this.matrix = new int[0][0];
	}
	
	/**
	 * Initializes a matrix with number of rows and columns equal to 'size',
	 * and with all entries set to zero.
	 * @param size
	 * 				the number of rows and columns for the matrix.
	 */
	public Matrix(int size) {
		this.matrix = new int[size][size];
		for( int row = 0; row < size; row++) {
			for( int col = 0; col < size; col++) {
				matrix[row][col] = 0;
			}
		}
	}
	
	/**
	 * Adds a value at the [row][col] and [col][row]
	 * @param val
	 * 				the value to put at the given location in the matrix.
	 * @param row
	 * 				the row index
	 * @param column
	 * 				the column index
	 * @requires that row >= 0 and column >= 0
	 * @return true if the value was successfully added to the matrix at
	 * 		   the given location, false otherwise.
	 */
	public boolean put(int val, int row, int column) {
		if( matrix.length > row && matrix.length > column ) {
			matrix[row][column] = val;
			matrix[column][row] = val;
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Returns an entry from this matrix given a row and column index.
	 * @param row
	 * 				the row index
	 * @param column
	 * 				the column index
	 * @requires row >= 0 and column >= 0
	 * @return the entry at the given location in the matrix if the
	 * 		   parameters are valid indices
	 * @throws InvalidArgumentException
	 */
	public int get(int row, int column) throws IllegalArgumentException {
		if( row >= size() || column >= size() ) {
			throw new IllegalArgumentException();
		}
		return matrix[row][column];
	}
	
	/**
	 * Increase the capacity of this matrix by one. In other words, increase
	 * the number of rows and columns by one. All additional entries created
	 * by this method are set to zero.
	 */
	public void increaseCapacity() {
		int[][] newMatrix = new int[matrix.length+1][matrix.length+1];
		for(int row = 0; row < matrix.length; row++) {
			for(int col = 0; col < matrix.length; col++) {
				newMatrix[row][col] = matrix[row][col];
			}
		}
		for(int i = 0; i < newMatrix.length; i++) {
			newMatrix[newMatrix.length-1][i] = 0;
			newMatrix[i][newMatrix.length-1] = 0; 
		}
		this.matrix = newMatrix;
	}
	
	/**
	 * Returns the size of the matrix. In other words, returns the number of
	 * rows and columns in this matrix.
	 * @return the size of the matrix
	 */
	public int size() {
		return matrix.length;
	}
	
	/**
	 * Checks whether an edge value exists at a given location. If an entry
	 * at matrix[i][j] = 0, then an edge value does not exist at (i,j). If an 
	 * entry at matrix[i][j] > 0, then an edge value does exist at (i,j).
	 * @param row
	 * 				the row index
	 * @param column
	 * 				the column index
	 * @return true if an edge exists at (i,j), false otherwise
	 */
	public boolean edgeExistsAt(int row, int column) {
		if( matrix[row][column] != 0 ) {
			return true;	// the edge exists
		}
		return false;
	}

	/**
	 * Returns an identical copy of this isntance of Matrix. 
	 * Used to conserve immutability.
	 * @return a copy of this matrix
	 */
	public Matrix clone() {
		Matrix newMatrix = new Matrix(this.size());
		for( int row = 0; row < this.size(); row++ ) {
			for( int col = 0; col < this.size(); col++ ) {
				newMatrix.put(this.matrix[row][col], row, col);
			}
		}
		return newMatrix;
	}
	
	/**
	 * Checks if two Matrix objects are representationally equivalent
	 */
	@Override
	public boolean equals(Object o) {
		if( !(o instanceof Matrix) || o.hashCode() != this.hashCode() ) {
			return false;
		}
		Matrix other = (Matrix) o;
		for( int i = 0; i < matrix.length; i++) {
			for( int j = 0; j < matrix.length; j++) {
				if( matrix[i][j] != other.get(i, j)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Hashcode for equality checking
	 */
	@Override
	public int hashCode() {
		return matrix.length;
	}
}
