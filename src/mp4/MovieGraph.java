package mp4;

import java.util.*;

/*
 * A MovieGraph contains a List of Movies and an adjacency matrix.
 * RI:	The index of a movieId in 'vertices' corresponds to its index in the adjacency matrix 'scoreMatrix'.
 * 		'vertices' cannot contain duplicates, in other words no two movieIds in 'vertices' m1 and m2 can exist
 * 		such that m1.equals(m2). 'scoreMatrix' must have the same number of rows as columns and the size of 
 * 		'vertices' must be equal to the number of rows and columns in 'scoreMatrix'. Vertices cannot be changed
 * 		after they are added to the graph.
 * AF:	MovieGraph represents a graph containing vertices representing movies and edges representing dissimilarity
 * 		scores between those movies. 'vertices' corresponds to the list of vertices in the graph. 'scoreMatrix' 
 * 		represents the dissimilarity scores between vertices. Let any movie that is a vertex be m1. 
 * 		Let the column vector with all entries zero except that at index = <index of m1 in 'vertices'> be M. Let the
 * 		matrix represented by 'scoreMatrix' be S. Then SM will equal a row vector such that for every non-zero entry e
 * 		at index i, the value of e will be equal to the dissimilarity score between m1 and the movie in the ith index of
 * 		'vertices'.
 */

public class MovieGraph {
	private Map<Integer, String> movieTitles;
	private ArrayList<Integer> vertices;
	private TreeMap<Integer,Movie> movies;
	private Matrix scoreMatrix;
	
	public MovieGraph() {
		this.vertices = new ArrayList<Integer>();
		this.scoreMatrix = new Matrix();
		this.movieTitles = new TreeMap<Integer, String>();
		this.movies = new TreeMap<Integer,Movie>();
	}
	
	/**
	 * Add a new movie to the graph. If the movie already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * movie to the graph and return true.
	 * 
	 * @param movie
	 *            the movie to add to the graph. Requires that movie != null.
	 * @return true if the movie was successfully added and false otherwise.
	 * @modifies this by adding the movie to the graph if the movie did not
	 *           exist in the graph.
	 */
	public boolean addVertex(Movie movie) {
		if( vertices.contains(movie.id()) ) {
			return false;
		}
		else {
			scoreMatrix.increaseCapacity();					// Increase capacity of matrix
			vertices.add(movie.id());						// Add movieId to list of vertices
			movieTitles.put(movie.id(), movie.getName());	// Add movieId and movieName to movieTitles
			movies.put(movie.id(),movie);
		}
		return true;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that m1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that m2 !=
	 *            null. Also require that m1 is not equal to m2 because
	 *            self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(Movie movie1, Movie movie2, int edgeWeight) {
		if( vertices.contains(movie1.id()) && vertices.contains(movie2.id())) {
			int index1 = vertices.indexOf(movie1.id());
			int	index2 = vertices.indexOf(movie2.id());
			if( movie1.id() == movie2.id() || scoreMatrix.edgeExistsAt(index1, index2) ) {
				return false;
			}
			return scoreMatrix.put(edgeWeight, index1, index2);
		}
		return false;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method should return false. Otherwise this method should add the
	 * edge to the graph and return true.
	 * 
	 * @param movieId1
	 *            the movie id for one end of the edge being added. Requires
	 *            that m1 != null.
	 * @param movieId2
	 *            the movie id for the other end of the edge being added.
	 *            Requires that m2 != null. Also require that m1 is not equal to
	 *            m2 because self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(int movieId1, int movieId2, int edgeWeight) {
		if( vertices.contains(movieId1) && vertices.contains(movieId2) ) {
			int index1 = vertices.indexOf(movieId1);
			int index2 = vertices.indexOf(movieId2);
			if( movieId1 == movieId2 || scoreMatrix.edgeExistsAt(index1, index2)) {
				return false;
			}
			else {
				return scoreMatrix.put(edgeWeight, index1, index2 );
			}
		}
		return false;
	}

	/**
	 * Return the length of the shortest path between two movies in the graph.
	 * Throws an exception if the movie ids do not represent valid vertices in
	 * the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the length of the shortest path between the two movies
	 *         represented by their movie ids.
	 */
	public int getShortestPathLength(int movieId1, int movieId2) throws NoSuchMovieException, NoPathException {
		if( !(vertices.contains(movieId1) && vertices.contains(movieId2)) ) {
			throw new NoSuchMovieException();
		}
		else {
			int source = movieId1;
			int target = movieId2;
			int current = source;
			int[] distances = new int[vertices.size()];
			int[] previous = new int[vertices.size()];
			Set<Integer> unvisited = new TreeSet<Integer>();
			distances[vertices.indexOf(source)] = 0;
			for( Integer id : vertices ) {
				if( id != source ) {
					distances[vertices.indexOf(id)] = Integer.MAX_VALUE;		// Set distances to 'infinity'
				}
				previous[vertices.indexOf(id)] = -1;						// Set previous to 'undefined'
				unvisited.add(id);
			}
			while(!unvisited.isEmpty()) {
				current = unvisitedVertexWithMinimumDistance(unvisited, distances);
				if( current == target ) {
					break;
				}
				if( unvisited.size() == 1 ) {
					throw new NoPathException();
				}
				unvisited.remove(current);
				for( int id : getNeighbours(current, unvisited)) {
					int tentativeDistance = distances[vertices.indexOf(current)] + scoreMatrix.get(vertices.indexOf(current), vertices.indexOf(id));
					if( tentativeDistance < distances[vertices.indexOf(id)]) {
						distances[vertices.indexOf(id)] = tentativeDistance;
						previous[vertices.indexOf(id)] = current;
					}
				}
			}
			return distances[vertices.indexOf(target)];
		}
	}

	/**
	 * Return the shortest path between two movies in the graph. Throws an
	 * exception if the movie ids do not represent valid vertices in the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 *  
	 * @return the shortest path, as a list, between the two movies represented
	 *         by their movie ids. This path begins at the movie represented by
	 *         movieId1 and ends with the movie represented by movieId2.
	 */
	public List<Movie> getShortestPath(int movieId1, int movieId2) throws NoSuchMovieException, NoPathException {
		if( !(vertices.contains(movieId1) && vertices.contains(movieId2)) ) {
			throw new NoSuchMovieException();
		}
		else {
			int source = movieId1;
			int target = movieId2;
			int current = source;
			int[] distances = new int[vertices.size()];
			int[] previous = new int[vertices.size()];
			Set<Integer> unvisited = new TreeSet<Integer>();
			distances[vertices.indexOf(source)] = 0;
			for( Integer id : vertices ) {
				if( id != source ) {
					distances[vertices.indexOf(id)] = Integer.MAX_VALUE;		// Set distances to 'infinity'
				}
				previous[vertices.indexOf(id)] = -1;						// Set previous to 'undefined'
				unvisited.add(id);
			}
			while(!unvisited.isEmpty()) {
				current = unvisitedVertexWithMinimumDistance(unvisited, distances);
				if( current == target ) {
					break;
				}
				unvisited.remove(current);
				for( int id : getNeighbours(current, unvisited)) {
					int tentativeDistance = distances[vertices.indexOf(current)] + scoreMatrix.get(vertices.indexOf(current), vertices.indexOf(id));
					if( tentativeDistance < distances[vertices.indexOf(id)]) {
						distances[vertices.indexOf(id)] = tentativeDistance;
						previous[vertices.indexOf(id)] = current;
					}
				}
			}
			List<Movie> pathList = getPathList( source, target, distances, previous);
			return pathList;
		}
	}
	
	/**
	 * Finds all unvisited neighbours of the given vertex and returns a list of movieIds of all neighbours.
	 * @param movieId - the MovieId of the vertex C whose neighbours to get
	 * @param unvisited - a list of unvisited vertices
	 * @return A List containing all the movieIds of the unvisited neighbours
	 */
	private List<Integer> getNeighbours(int movieId, Set<Integer> unvisited) {
		List<Integer> neighbours = new ArrayList<Integer>();		// movieIds
		int index = vertices.indexOf(movieId);
		for(int i = 0; i < vertices.size(); i++) {
			if(scoreMatrix.get(index, i) != 0 && unvisited.contains(vertices.get(i))) {
				neighbours.add(vertices.get(i));		// If vertex is an unvisited neighbour, add to list of neighbours
			}
		}
		return neighbours;
	}
	
	/**
	 * Finds the vertex in 'unvisited' with the smallest distance and returns its movieId
	 * @param unvisited
	 * 				A set of all unvisited vertices, each given by their movieIds
	 * @param distances
	 * 				An array of all distances from the source vertex to the given vertex, 
	 * 				ordered such that indices of vertices in 'vertices' and in 'distances'
	 * 				are the same for every vertex.
	 * @return The movieId of the unvisited vertex with the smallest assigned distance.
	 */
	private int unvisitedVertexWithMinimumDistance(Set<Integer> unvisited, int[] distances) {
		int min = Integer.MAX_VALUE;
		int minId = -1;
		for( int id : unvisited ) {
			if( distances[vertices.indexOf(id)] < min ) {
				minId = id;
				min = distances[vertices.indexOf(id)];
			}
		}
		return minId;
	}
	
	/**
	 * Returns the sequential path of vertices as a list of movies from source to target
	 * given the info for the path.
	 * @param source
	 * 				MovieId of the source vertex.
	 * @param target
	 * 				MovieId of the target vertex.
	 * @param distances
	 * 				An array of all distances from the source vertex to the given vertex, 
	 * 				ordered such that indices of vertices in 'vertices' and in 'distances'
	 * 				are the same for every vertex.
	 * @param previous
	 * 				An array of all previous vertices along the given path for each vertex,
	 * 				ordered such that indices of vertices in 'vertices' and in 'distances'
	 * 				are the same for every vertex.
	 * @return A list of movies corresponding to the sequential path from source to target.
	 */
	private List<Movie> getPathList( int source, int target, int[] distances, int[] previous) {
		List<Movie> pathList = new ArrayList<Movie>();
		Stack<Integer> pathStack = new Stack<Integer>();
		int current = target;
		pathStack.push(current);
		do {
			current = previous[vertices.indexOf(current)];
			pathStack.push(current);
		} while (previous[vertices.indexOf(current)] != -1);
		while(!pathStack.isEmpty()) {
			pathList.add(movies.get(pathStack.pop()));
		}
		return pathList;
	}
	
	/**
	 * Returns the movie id given the name of the movie. For movies that are not
	 * in English, the name contains the English transliteration original name
	 * and the English translation. A match is found if any one of the two
	 * variations is provided as input. Typically the name string has <English
	 * Translation> (<English Transliteration>) for movies that are not in
	 * English.
	 * 
	 * @param name
	 *            the movie name for the movie whose id is needed.
	 * @return the id for the movie corresponding to the name. If an exact match
	 *         is not found then return the id for the movie with the best match
	 *         in terms of translation/transliteration of the movie name.
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph.
	 */
	public int getMovieId(String name) throws NoSuchMovieException {
		for( Integer id : vertices ) {				// First, check for exact matches
			if( movieTitles.get(id).equals(name)  ) {
				return id;
			}
		}
		for( Integer id : vertices ) {				// If no exact matches are found, search for partial matches
			if( movieTitles.get(id).contains(name) ) {
				return id;
			}
		}
		throw new NoSuchMovieException();			// If no exact or partial matches are found, throw an exception
	}
	
	/**
	 * Used for checking equality of different MovieGraph objects
	 * @return a clone of this MovieGraph's adjacency matrix
	 */
	public Matrix getMatrix() {
		return this.scoreMatrix.clone();
	}
	
	/**
	 * Used for checking equality of different MovieGraph objects
	 * @return a clone of this MovieGraph's vertex list
	 */
	public ArrayList<Integer> getVertices() {
		ArrayList<Integer> newVertices = new ArrayList<Integer>();
		for( int i = 0; i < scoreMatrix.size(); i++ ) {
			newVertices.add(vertices.get(i));
		}
		return newVertices;
	}
	
	@Override
	public boolean equals(Object other) {
		if( !(other instanceof MovieGraph) || this.hashCode() != other.hashCode() ) {
			return false;
		}
		MovieGraph otherMovieGraph = (MovieGraph) other;
		Matrix otherMatrix = otherMovieGraph.getMatrix();
		if( this.scoreMatrix.equals(otherMatrix) && this.vertices.equals(otherMovieGraph.getVertices()) ) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return vertices.size();
	}
}
