package mp4;

import static org.junit.Assert.*;
import java.util.*;
import java.io.IOException;
import org.junit.Test;
import org.junit.Before;

//**********************************************************************************************//
//******************************************IMPORTANT*******************************************//
//**********************************************************************************************//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This test class tests cases using the large data set provided. See MovieGraphSmallTest.java *
 * for more robust testing using a more tangible data set.									   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
//**********************************************************************************************//
//**********************************************************************************************//
//**********************************************************************************************//

public class MovieGraphTest {
	MovieGraph mg, smg;
	Map<Integer,List<Rating>> ratingMap;
	List<Rating> ratings;
	List<Movie> movies;
	
	@Before
	public void setupLargeDataSet() throws IOException {
		mg = new MovieGraph();
		ratingMap = new TreeMap<Integer, List<Rating>>();
		ratings = new ArrayList<Rating>();
		movies = new ArrayList<Movie>();
		MovieIterator iter = new MovieIterator("data/u.item.txt");
		Movie nextMovie;
		while ( iter.hasNext() ) {
			nextMovie = iter.getNext();
			mg.addVertex(nextMovie);
			movies.add(nextMovie);
		}
				
		RatingIterator iter2 = new RatingIterator("data/u.data.txt");
		while (iter2.hasNext()) {
			ratings.add(iter2.getNext());
		}
		
		for(Movie m : movies) {
			if( movies.indexOf(m) % 100 == 0 )
				System.out.println(movies.indexOf(m));
			List<Rating> mRatings = new ArrayList<Rating>();
			for(Rating r : ratings) {
				if(r.getMovieId() == m.id()) {
					mRatings.add(r);
				}
			}
			ratingMap.put(m.id(), mRatings);
 		}
		
		List<Rating> m1Ratings;
		List<Rating> m2Ratings;
		Movie m2;
		int i = 0;
		for( Movie m1 : movies) {
			if( movies.indexOf(m1) % 100 == 0 ) 
				System.out.println(movies.indexOf(m1));
			m1Ratings = ratingMap.get(m1.id());
			i++;
			for(int j = i; j < movies.size(); j++) {
				int likerIntersection = 0;
				int dislikerIntersection = 0;
				m2 = movies.get(j);
				m2Ratings = ratingMap.get(m2.id());
				int allReviewers = m2Ratings.size() + m1Ratings.size();
				for( Rating r1 : m1Ratings ) {
					for( Rating r2 : m2Ratings ) {
						if( r1.getUserId() == r2.getUserId() ) {
							if( r1.getRating() > 3 && r2.getRating() > 3 ) {
								likerIntersection++;
							}
							else if( r1.getRating() < 3 && r2.getRating() < 3 ) {
								dislikerIntersection++;
							}
						}
					}
				}
				int edgeWeight = 1 + allReviewers - (likerIntersection + dislikerIntersection);
				mg.addEdge(m1,m2,edgeWeight);
			}
		}
	}
	
	@Test
	public void testPathLengthLargeDataSet() {
		try {
			int path1 = mg.getShortestPathLength(movies.get(1).id(), movies.get(2).id());
			System.out.println("Path 1: " + path1);
			assertTrue(path1 > 0);
			int path2 = mg.getShortestPathLength(movies.get(28).id(), movies.get(448).id());
			System.out.println("Path 2: " + path2);
			assertTrue(path2 > 0);
		} catch (NoSuchMovieException e) {
			fail();
		} catch (NoPathException e) {
			System.out.println("No path...");
		}
	}
	
	@Test public void testShortestPathLargeDataSet() {
		try {
			List<Movie> path3 = mg.getShortestPath(movies.get(32).id(), movies.get(559).id());
			System.out.println("Path 3: " + path3);
			assertTrue(path3 != null);
			List<Movie> path4 = mg.getShortestPath(movies.get(42).id(), movies.get(442).id());
			System.out.println("Path 4: " + path4);
			assertTrue(path4 != null);
		} catch (NoSuchMovieException e ) {
			fail();
		} catch (NoPathException e ) {
			System.out.println("No path");
		}
	}
	
}
