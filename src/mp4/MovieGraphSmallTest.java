package mp4;

import static org.junit.Assert.*;
import java.util.*;
import org.junit.Test;
import org.junit.Before;

public class MovieGraphSmallTest {
	MovieGraph mg;
	ArrayList<Movie> movies;
	
	/**
	 * This method sets up a movieGraph like the diagram at http://en.wikipedia.org/wiki/Dijkstra's_algorithm
	 * for simplicity in testing.
	 */
	@Before
	public void setup() {
		mg = new MovieGraph();
		movies = new ArrayList<Movie>();
		for( int i = 1; i <= 6; i++) {
			Movie m = new Movie(i, String.valueOf(i), 2000 + i, String.valueOf(i));
			movies.add(m);
			mg.addVertex(m);
		}
		mg.addEdge(1,2,7);
		mg.addEdge(1,3,9);
		mg.addEdge(1,6,14);
		mg.addEdge(2,3,10);
		mg.addEdge(2,4,15);
		mg.addEdge(4,3,11);
		mg.addEdge(4,5,6);
		mg.addEdge(5,6,9);
		mg.addEdge(6,3,2);
	}
	
	/**
	 * Tests the shortest path length method (int) in the MovieGraph class.
	 */
	@Test
	public void testShortestPathLength() {
		try {
			int path15 = mg.getShortestPathLength(1, 5);
			assert(path15 == 20);
			System.out.println("Path 1 => 5: " + path15);
			int path26 = mg.getShortestPathLength(2, 6);
			assert(path26 == 12);
			System.out.println("Path 2 => 6: " + path26);
		} catch (NoSuchMovieException e) {
			fail();
			e.printStackTrace();
		} catch (NoPathException e) {
			fail();
			e.printStackTrace();
		}
	}
	
	/**
	 * Tests the shortest path method (List<Movie>) in MovieGraph class.
	 */
	@Test
	public void testShortestPath() {
		try {
			List<Movie> path15 = mg.getShortestPath(1, 5);
			assert(path15.size() == 4);
			System.out.println("Path 1 => 5: " + path15);
			List<Movie> path26 = mg.getShortestPath(2, 6);
			assert(path26.size() == 3);
			System.out.println("Path 2 => 6: " + path26);
		} catch (NoSuchMovieException e) {
			fail();
		} catch (NoPathException e) {
			fail();
			e.printStackTrace();
		}
	}
	
	/**
	 * Tests the addEdge method of MovieGraph.
	 */
	@Test
	public void addEdgeTest() {
		// Try adding an edge where we have already assigned one (1,2).
		// This should return false and the edge should not be added.
		assertFalse(mg.addEdge(1, 2, 42));
		assertFalse(mg.addEdge(movies.get(0), movies.get(1), 42));
		// Try adding an edge between a vertex and itself.
		// This should return false because self-reference is not meaningful
		// for this implementation.
		assertFalse(mg.addEdge(5, 5, 44));
		assertFalse(mg.addEdge(movies.get(4), movies.get(4), 44));
	}
	
	/**
	 * Tests the addVertex method of MovieGraph.
	 */
	@Test
	public void addVertexTest() {
		// Try adding a movie that has already been added to the graph.
		// This should return false because vertices should be unique.
		assertFalse(mg.addVertex(movies.get(0)));
	}

}
